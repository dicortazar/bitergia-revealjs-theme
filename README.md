# Bitergia theme for reveal.js

This repository contains a [.css file](css/bitergia.css) created for 
[reveal.js](https://github.com/hakimel/reveal.js) to have a *Bitergian* style 
template for presentations.

![screenshot](screenshot.jpg)

## How to use it

In a reveal.js based presentation, include `bitergia.css`:

```html
<!-- Bitergia custom theme -->
<link rel="stylesheet" href="./css/bitergia.css">
```

To add only the bitergia logo add `bitergia` class to `body` element:
```html
<body class="bitergia">
```

To add both, the bitergia logo and the Creative Commons by-sa logo, add
`bitergia-cc` class:
```html
<body class="bitergia-cc">
```

There is a couple of files included to test it:
* [slides.html](slides.html), that is the core html to do the presentation.
* [example.md](example.md), as slides content in markdown. You can use direct html in `slides.html` if you prefer.

# How to create or modify the theme

You can create your own theme files very easy with reveal.js

1. Fetch reveal.js module
1. Install dependencies
```shell
$ cd reveal.js
reveal.js$ npm install
```

1. In `css/theme/source` create a `.scss` file or modify one of the existing ones
1. Create the final `.css` file with `grunt css-themes`:
```shell
reveal.js$ grunt css-themes
```

1. The new file should be in `css/theme/`

# License

MIT licensed

Copyright (C) 2017 J. Manrique López, http://jsmanrique.es